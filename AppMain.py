import Player
import Labyrinth
import pygame
import Weapon
import Colors
import Button
import random
import datetime
from pygame.locals import *


class App:
    width = 800
    height = 600
    player = None
    ennemies = list()
    clock = pygame.time.Clock()

    def __init__(self):
        self._running = True
        self._display_surf = None
        self.freeTile = None
        self.wallTile = None
        self.lifepointsShow = None
        self.moneyShow = None
        self.font = None
        self.color = Colors.Color()

        lab = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               3, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1,
               1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1,
               1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1,
               1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1,
               1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1,
               1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1,
               1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1,
               1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1,
               1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1,
               1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1,
               1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1,
               1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1,
               1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1,
               1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1,
               1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1,
               1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1,
               1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1,
               1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1,
               1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1]

        self.maze = Labyrinth.Labyrinth(lab)
        self.player = Player.player("Ressources/worker.png")
        self.player.Tile = self.maze.Lab[20]
        self.player.weapon = Weapon.Weapon(0,5)
        self.ennemies = self.maze.initEnnemies()
        self.playerHit = None
        self.ennemyHit = None
        self.finnishSound = None

    def intro(self):
        intro = True
        while intro:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    quit()
            self._display_surf.fill(self.color.Black)
            TitleText = pygame.font.SysFont('Arial',50)
            creditsText = pygame.font.SysFont('Arial',16)
            textSurf = TitleText.render("Aegeus' Gladiator",True,self.color.White)
            creditsTextSurf = creditsText.render("by TheShamo",True,self.color.White)
            textRect = textSurf.get_rect()
            textRect2 = creditsTextSurf.get_rect()
            textRect.center = (int(self.width/2),int(self.height/4))
            textRect2.center = (750,575)
            self._display_surf.blit(textSurf,textRect)
            self._display_surf.blit(creditsTextSurf, textRect2)

            if Button.newButton(self._display_surf,300,250,50,200,
                             self.color.White,self.color.Black,"Play !",self.color.Grey):
                intro = False
                self.on_execute()

            pygame.display.update()
            self.clock.tick(15)


    def on_init(self):
        pygame.init()
        pygame.mixer.init()
        self._display_surf = pygame.display.set_mode((self.width, self.height), pygame.HWSURFACE)
        self.font = pygame.font.SysFont('Arial', 22)
        pygame.display.set_caption("Aegeus' Gladiator")
        self._running = True
        self.playerHit = pygame.mixer.Sound("Ressources/hit.wav")
        self.ennemyHit = pygame.mixer.Sound("Ressources/ennemy_hit.wav")
        self.finnishSound = pygame.mixer.Sound("Ressources/finish.wav")

    def on_event(self,event,keys):
        if event.type == pygame.QUIT:
            self._running = False
        if event.type == pygame.KEYUP:
            if keys[K_RIGHT]:
                self.player.moveRight()
            if keys[K_LEFT]:
                self.player.moveLeft()
            if keys[K_UP]:
                self.player.moveUp()
            if keys[K_DOWN]:
                self.player.moveDown()
            if keys[K_SPACE]:
                pygame.mixer.Sound.play(self.playerHit)
                for ennemy in self.ennemies:
                    if ennemy.Tile.id == self.player.Tile.tileL.id:
                        ennemy.takeDamages(self.player.attack())
                    elif ennemy.Tile.id == self.player.Tile.tileT.id:
                        ennemy.takeDamages(self.player.attack())
                    elif ennemy.Tile.id == self.player.Tile.tileR.id:
                        ennemy.takeDamages(self.player.attack())
                    elif ennemy.Tile.id == self.player.Tile.tileD.id:
                        ennemy.takeDamages(self.player.attack())
                    if ennemy.lifepoints <= 0:
                        self.player.money += 10
                        self.ennemies.remove(ennemy)
            if keys[K_ESCAPE]:
                self._running = False

    def on_loop(self):
        self.lifepointsShow = self.font.render("Lifepoints " + str(self.player.lifepoints), True, self.color.White)
        self.moneyShow = self.font.render("Money " + str(self.player.money), True, self.color.White)
        pass

    def on_render(self):
        self._display_surf.fill((0, 0, 0))

        self.maze.draw(self._display_surf)
        self.maze.fogofwar(self.getVisibleTiles(self.player.Tile), self._display_surf)
        for ennemy in self.ennemies:
            if ennemy.Tile in self.getVisibleTiles(self.player.Tile):
                self._display_surf.blit(ennemy.bg_image,(ennemy.Tile.coordx + 5, ennemy.Tile.coordy + 5))
        self._display_surf.blit(self.player.bg_image, (self.player.Tile.coordx + 5, self.player.Tile.coordy + 5))

        self._display_surf.blit(self.lifepointsShow,(625,50))
        self._display_surf.blit(self.moneyShow, (625, 100))

        pygame.draw.line(self._display_surf,self.color.White,(20*30,0),(20*30,20*30),2)
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() is False:
            self._running = False

        while self._running:
            keys = pygame.key.get_pressed()

            if self.player.lifepoints <= 0:
                self.intro()
            try:
                for event in pygame.event.get():
                    self.on_event(event,keys)
            except AttributeError:
                print("")

            if self.player.Tile.tileType == 4:
                self.winning()
                return 0

            self.actionEnnemies()
            self.on_loop()
            self.on_render()
        self.on_cleanup()

    def actionEnnemies(self):
        for ennemy in self.ennemies:
            if ennemy.canAct(datetime.datetime.now().second):
                if self.player.Tile.id in (ennemy.Tile.tileL.id,ennemy.Tile.tileR.id,ennemy.Tile.tileT.id,ennemy.Tile.tileD.id):
                    self.player.takeDamages(ennemy.attack())
                    pygame.mixer.Sound.play(self.ennemyHit)
                else:
                    x = random.randint(0,3)
                    if x == 0:
                        ennemy.moveLeft()
                    if x == 1:
                        ennemy.moveUp()
                    if x == 2:
                        ennemy.moveRight()
                    if x == 3:
                        ennemy.moveDown()

    def getVisibleTiles(self,tile):
        res = list()
        try:
            res += tile.getRightTile(2)
            res += tile.getLeftTile(2)
            res += tile.getDownTile(2)
            res += tile.getTopTile(2)
        except AttributeError:
            print("")
        return res

    def loosing(self):
        print("loosing")

    def winning(self):
        pygame.mixer.Sound.play(self.finnishSound)
        if self.player.money < 50:
            print("You don't have enough money to win, sell a bit of your lifepoints")
            while self.player.money < 50:
                self.player.lifepoints -= 1
                self.player.money += 10
        self.player.Tile = self.maze.Lab[20]
        self.intro()




