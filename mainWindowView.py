import AppMain

if __name__ == "__main__" :
    theApp = AppMain.App()
    theApp.on_init()
    theApp.intro()
    theApp.on_execute()
