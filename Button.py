import pygame
import Colors


def newButton(_display_surf,x,y,height,width,bg_color,fr_color,text,h_color):
    pygame.draw.rect(_display_surf, bg_color, (x, y, width, height))
    if x + width > pygame.mouse.get_pos()[0] > x and y + height > pygame.mouse.get_pos()[1] > y:
         pygame.draw.rect(_display_surf, h_color, (x+5, y+5, width-10, height-10))
         if pygame.mouse.get_pressed()[0] == 1:
            return 1
    else:
        pygame.draw.rect(_display_surf, fr_color, (x+5, y+5, width-10, height-10))

    color = Colors.Color()
    menuText = pygame.font.SysFont('Arial', 22)
    textSurf = menuText.render("Play !", True, color.White)
    textRect = textSurf.get_rect()
    textRect.center = ((x + (width / 2)), (y + (height / 2)))
    _display_surf.blit(textSurf, textRect)
