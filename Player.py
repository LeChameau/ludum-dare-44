import pygame

class player:
    def __init__(self, bgimagePath):
        self.lifepoints = 20
        self.equipments = dict()
        self.members = list()
        self.Tile = None
        self.bg_image = pygame.image.load(bgimagePath)
        self.money = 0
        self.weapon = None

    def moveRight(self):
        if self.Tile.tileR.tileType != 1:
            self.Tile = self.Tile.tileR

    def moveLeft(self):
        if self.Tile.tileL.tileType != 1:
            self.Tile = self.Tile.tileL

    def moveUp(self):
        if self.Tile.tileT.tileType != 1:
            self.Tile = self.Tile.tileT

    def moveDown(self):
        if self.Tile.tileD.tileType != 1:
            self.Tile = self.Tile.tileD

    def takeDamages(self,dammages):
        self.lifepoints -= dammages

    def attack(self):
        return self.weapon.damages
