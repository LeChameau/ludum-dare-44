class Tile:
    def __init__(self,id,tileType):
        self.id = id
        self.tileType = tileType
        self.tileL = None
        self.tileT = None
        self.tileR = None
        self.tileD = None
        self.coordx = 0
        self.coordy = 0

    def getRightTile(self,n):
        res = list()
        if n == 1:
            res.append(self.tileT)
            res.append(self.tileD)
            res.append(self)
            return res
        res.append(self)
        return res + self.tileR.getRightTile(n-1)

    def getLeftTile(self,n):
        res = list()
        if n == 1:
            res.append(self.tileT)
            res.append(self.tileD)
            res.append(self)
            return res
        res.append(self)
        return res + self.tileL.getLeftTile(n - 1)

    def getTopTile(self,n):
        res = list()
        if n == 1:
            res.append(self.tileL)
            res.append(self.tileR)
            res.append(self)
            return res
        res.append(self)
        return res + self.tileT.getTopTile(n - 1)

    def getDownTile(self,n):
        res = list()
        if n == 1:
            res.append(self.tileL)
            res.append(self.tileR)
            res.append(self)
            return res
        res.append(self)
        return res + self.tileD.getDownTile(n - 1)

