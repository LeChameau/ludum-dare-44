import pygame

class Ennemy:
    def __init__(self):
        self.Tile = None
        self.lifepoints = 10
        self.weapon = None
        self.bg_image = pygame.image.load("Ressources/ennemy.png")
        self.sec = 0

    def moveRight(self):
        if not self.Tile.tileR.tileType:
            self.Tile = self.Tile.tileR

    def moveLeft(self):
        if not self.Tile.tileL.tileType:
            self.Tile = self.Tile.tileL

    def moveUp(self):
        if not self.Tile.tileT.tileType:
            self.Tile = self.Tile.tileT

    def moveDown(self):
        if not self.Tile.tileD.tileType:
            self.Tile = self.Tile.tileD

    def attack(self):
        return self.weapon.damages

    def canAct(self,sec):
        res = self.sec != sec
        self.sec = sec
        return res

    def takeDamages(self,damages):
        self.lifepoints -= damages



