import Tile
import pygame
import Ennemy
import random
import Weapon


class Labyrinth:
    def __init__(self, lab):
        self.freeTile = pygame.image.load("Ressources/grey_tile.png")
        self.wallTile = pygame.image.load("Ressources/brown_tile.png")
        self.blackTile = pygame.image.load("Ressources/black_tile.png")
        self.Lab = dict()
        bx = 0
        by = 0
        for i in range(0, len(lab)):
            tmp = Tile.Tile(i, lab[bx + (by * 20)])
            tmp.coordx = bx * 30
            tmp.coordy = by * 30
            if bx!=0:
                self.Lab[tmp.id-1].tileR = tmp
                tmp.tileL = self.Lab[tmp.id-1]
            if by != 0:
                self.Lab[(tmp.id - 20)].tileD = tmp
                tmp.tileT = self.Lab[tmp.id - 20]

            bx += 1
            if bx > 20 - 1:
                bx = 0
                by += 1
            self.Lab[tmp.id] = tmp

    def draw(self, display_surf):
        bx = 0
        by = 0
        for k in self.Lab.keys():
            if self.Lab[k].tileType == 1:
                display_surf.blit(self.wallTile, (self.Lab[k].coordx, self.Lab[k].coordy))
            if self.Lab[k].tileType in (0,3,4):
                display_surf.blit(self.freeTile, (self.Lab[k].coordx, self.Lab[k].coordy))

            bx += 1
            if bx > 20 - 1:
                bx = 0
                by += 1

    def initEnnemies(self):
        ennemies = list()
        for i in range(0,5):
            tmp = Ennemy.Ennemy()
            x = random.randint(0,399)
            while self.Lab[x].tileType != 0:
                x = random.randint(0, 399)
            tmp.Tile = self.Lab[x]
            tmp.weapon = Weapon.Weapon(1,5)
            ennemies.append(tmp)
        return ennemies

    def fogofwar(self,tiles,display_surf):
        for tile in self.Lab.values():
            if tile not in tiles:
                display_surf.blit(self.blackTile, (tile.coordx, tile.coordy))

